import React, { useState } from "react";
import styled from "styled-components";

// Styled components
const AppContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 40px;
`;

const InputContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-bottom: 20px;
`;

const InputLabel = styled.label`
  font-size: 16px;
  font-weight: bold;
  margin-bottom: 5px;
`;

const Input = styled.input`
  width: 200px;
  height: 25px;
  font-size: 16px;
  margin-bottom: 15px;
`;

const ResultContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 20px;
`;

const ResultLabel = styled.div`
  font-size: 20px;
  font-weight: bold;
  margin-bottom: 10px;
`;

const ResultValue = styled.div`
  font-size: 18px;
  margin-bottom: 5px;
`;

const Button = styled.button`
  width: 120px;
  height: 35px;
  font-size: 16px;
  background-color: #008000;
  color: #fff;
  border: none;
  border-radius: 5px;
  margin-top: 20px;
  cursor: pointer;

  &:hover {
    background-color: #005200;
  }
`;

// Main component
function App() {
  // Input state
  const [sunshine, setSunshine] = useState(1000);
  const [area, setArea] = useState(50);
  const [efficiency, setEfficiency] = useState(0.18);
  const [energyPrice, setPrice] = useState(0.12);
  const [initialCost, setCost] = useState(20000);

  // Result state
  const [paybackTime, setPaybackTime] = useState(0);

  function calculateAnnualProduction(nasloniecznienie, wielkoscInstalacji, sprawnosc) {
    return nasloniecznienie * wielkoscInstalacji * sprawnosc;
  }
  
  function calculateAnnualSavings(annualProduction, cenaPradu) {
    return annualProduction * cenaPradu;
  }

  
  function calculatePaybackPeriod(initialCost, annualSavings) {
    return initialCost / annualSavings;
  }

  // Event handler
  const calculateResults = () => {
  const sunshine = 1000; // kWh/m^2/rok
  const area = 50; // m^2
  const efficiency = 0.18; // Efektywność paneli fotowoltaicznych (jako wartość dziesiętna)
  const energyPrice = 0.12; // Cena prądu za kWh (w USD)

  const initialCost = 20000; // Koszt instalacji fotowoltaicznej (w USD)

  const annualProduction = calculateAnnualProduction(sunshine, area, efficiency);
  const annualSavings = calculateAnnualSavings(annualProduction, energyPrice);
  const paybackPeriod = calculatePaybackPeriod(initialCost, annualSavings);
    setPaybackTime(paybackPeriod);
  };

  return (
    <AppContainer>
      <h1>Kalkulator instalacji fotowoltaicznej</h1>
      <InputContainer>
        <InputLabel>Nasłonecznienie (kWh/m^2/rok):</InputLabel>
        <Input
          type="number"
          value={sunshine}
          onChange={(event) => setSunshine(Number(event.target.value))}
        />
        <InputLabel>Rozmiar instalacji (m^2):</InputLabel>
        <Input
          type="number"
          value={area}
          onChange={(event) => setArea(Number(event.target.value))}
        />
        <InputLabel>Sprawność:</InputLabel>
        <Input
          type="number"
          value={efficiency}
          onChange={(event) => setEfficiency(Number(event.target.value))}
        />
        <InputLabel>Cena prądu za kWh:</InputLabel>
        <Input
          type="number"
          value={energyPrice}
          onChange={(event) => setPrice(Number(event.target.value))}
        />
        <InputLabel>Koszt całkowity:</InputLabel>
        <Input
          type="number"
          value={initialCost}
          onChange={(event) => setCost(Number(event.target.value))}
        />
        <Button onClick={calculateResults}>Oblicz</Button>
      </InputContainer>
      { paybackTime > 0 && (
        <ResultContainer>
          <ResultLabel>Rezultat:</ResultLabel>
          <ResultValue>Czas zwrotu: {paybackTime.toFixed(2)} lat</ResultValue>
        </ResultContainer>
      )}
    </AppContainer>
    );
  }
  
  export default App;